<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\user\model
{
	use nuclio\plugin\database\orm\Model;
	
	/**
	 * @Collection userRole
	 * 
	 * User role data model 
	 * 
	 * represent user role collection in database
	 */
	class UserRole extends Model
	{
		/**
		 * @Id(strategy="AUTO")
		 * 
		 * @var        mixed $id 	User role ID
		 * 
		 * @access public
		 */
		public mixed $id=null;
		
		/**
		 * @relate nuclio\plugin\user\model\User
		 *
		 * @var        Model|null $user 	user object
		 *                         
		 * @access public
		 */
		public ?Model $user=null;
		
		/**
		 * @relate nuclio\plugin\auth\model\Role
		 *
		 * @var        Model|null $role 	User role object
		 * 
		 * @access public
		 */
		public ?Model $role=null;
	}
}
