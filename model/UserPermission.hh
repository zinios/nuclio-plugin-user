<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\user\model
{
	use nuclio\plugin\database\orm\Model;
	
	/**
     * @Collection userPermission 
	 *
	 * User permission data model 
	 * 
	 * represent user permission collection in database
	 */
	class UserPermission extends Model
	{
		/**
		 * @Id(strategy="AUTO")
		 *
		 * @var        mixed $id 	User permission ID
		 * 
		 * @access public
		 */
		public mixed $id=null;
		
		/**
		 * @relate nuclio\plugin\user\model\User 
		 *
		 * @var        Model|null $user 	user object
		 *                         
		 * @access public
		 */
		public ?Model $user=null;
		
		/**
		 * @relate nuclio\plugin\auth\model\Permission
		 *
		 * @var        Model|null $permission 	permission object
		 *                         
		 * @access public
		 */
		public ?Model $permission=null;
	}
}
