<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\user\model
{
	use nuclio\plugin\database\orm\Model;
	
	/**
	 * @Collection user
	 *
	 * User data model 
	 * 
	 * represent user collection in database
	 */
	class User extends Model
	{
		/**
		 * @Id(strategy="AUTO")
		 */
		public mixed $id=null;
		
		/**
		 * @Type varchar
		 * @Length 256
		 */
		public ?string $ref=null;
		
		/**
		 * @Type varchar
		 * @Length 256
		 */
		public ?string $email=null;
		
		/**
		 * @Type varchar
		 * @Length 256
		 */
		public ?string $password=null;
		
		/**
		 * @Type varchar
		 * @Length 256
		 */
		public ?string $salt=null;
		
		/**
		 * @Type varchar
		 * @Length 256
		 */
		public ?string $tags=null; 
		
		/**
		 * @Type varchar
		 * @Length 256
		 */
		public ?string $verificationCode=null; 
		
		/**
		 * @Type varchar
		 * @Length 256
		 */
		public ?string $resetPasswordCode=null; 
		
		/**
		 * @Type int
		 * @Length 1
		 */
		public ?int $status=null;
	}
}
