<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\user
{ 
	use nuclio\core\
	{
		plugin\Plugin,
		ClassManager
	};
	use nuclio\plugin\user\
	{
		model\User as UserModel,
		model\UserRole as UserRoleModel,
		model\UserPermission as UserPermissionModel,
		UserException
	};
	use nuclio\plugin\
	{
		email\Email,
		database\datasource\manager\Manager as DataSourceManager,
		database\datasource\source\Source,
		config\ConfigCollection,
		config\Config
	};
	
	/**
	 * it is responsible for User maintenance functionality like 
	 * Create new user, Update user, validate user data and etc.
	 * 
	 * @package    nuclio\plugin\user
	 */
	<<
		__ConsistentConstruct,
		singleton
	>>
	class User extends Plugin
	{
		const STATUS_CONFIRMED		=1;

		/**
		 * @var        ConfigCollection $config 	Keep configs data
		 * @access private
		 */
		private ConfigCollection $config;

		/**
		 * @var        Source $dataSourceManager 	Keep data source instance
		 * @access private
		 */
		private Source $dataSourceManager;

		/**
		 * Static method to create an instance of User class 
		 * 
		 * Create an instance of User class if there is no existing one
		 * 
		 * @access public
		 * @return     User An instance of User class
		 */
		public static function getInstance(/* HH_FIXME[4033] */...$args):this
		{
			$instance=ClassManager::getClassInstance(static::class,...$args);
			return ($instance instanceof static)?$instance:new static(...$args);
		}

		/**
		 * User class constructor
		 * 
		 * Set config data and get and instance of data source
		 * 
		 * @param      ConfigCollection $config
		 * @access public
		 */
		public function __construct(ConfigCollection $config)
		{
			$this->config=$config;
			$this->dataSourceManager=DataSourceManager::getSource((string)$config->get('dataSource'));
			parent::__construct();
		}

		/**
		 * Create new user
		 * 
		 * @access public
		 * @param      Map<string,mixed> $record Data of user to be created
		 * @return     UserModel returned created user
		 */
		public function create(Map<string,mixed> $record):UserModel
		{
			$this->insertValidation($record);

			$email		=(string)$record->get('email');
			$password	=(string)$record->get('password');
			$ref		=(string)$record->get('ref');
			$status		=$record->get('status');
			$set_random	=$record->get('set_random');
			$roles		=$record->get('roles');
			$permissions=$record->get('permissions');
			$tags 		=$record->get('tags');
			$salt		=$this->generateSalt();

			$user=$this->createUser();
			$user->setEmail($email);
			$user->setRef($ref);
			//to identify if the user is admin user or from the website user
			$user->setTags($tags);

			if($set_random==true)
			{
				$randomPassword=$this->generateRandomPassword();
				$password=md5($salt.$randomPassword);
				$user->setPassword($password);
				$user->setStatus(self::STATUS_CONFIRMED);
				$this->sendEmail($email,'New Account Created','This is your password '.$randomPassword);
			}
			else
			{
				$password=md5($salt.$password);
				$user->setPassword($password);
				$user->setStatus($status);
			}
			$user->setSalt($salt);
			$user->save();
			
			if (is_array($roles) && count($roles))
			{
				$this->createUserRoles($user, $roles);
			}
			if (is_array($permissions) && count($permissions))
			{
				$this->createUserPermissions($user, $permissions);
			}
			
			return $user;
		}
		
		protected function createUser():UserModel
		{
			return UserModel::create();
		}
		
		protected function findById(string $id):UserModel
		{
			return UserModel::findById($id);
		}
		
		protected function createUserRole():UserRoleModel
		{
			return UserRoleModel::create();
		}
		
		protected function createUserPermission():UserPermissionModel
		{
			return UserPermissionModel::create();
		}

		/**
		 * Update existing user
		 * 
		 * @access public
		 * @param   Map<string,mixed> $record User data to be update
		 * @return  void
		 */
		public function update(Map<string,mixed> $record):void
		{
			$this->validate($record);
			$id=$record->get('id');
			$user=$this->findById($id);
			
			if (!$user)
			{
				throw new UserException('Invalid user. The user probably doesn\'t exist anymore.',3);
			}

			$email		=(string)$record->get('email');
			$ref		=(string)$record->get('ref');
			$password	=(string)$record->get('password');
			$status		=$record->get('status');
			$set_random	=$record->get('set_random');
			$roles		=$record->get('roles');
			$permissions=$record->get('permissions');
			$tags=$record->get('tags');

			$salt = $this->generateSalt();

			$user->setEmail($email);
			$user->setRef($ref);
			$user->setStatus($status);
			if ($set_random==true)
			{
				$password=$this->generateRandomPassword();
				$password=md5($salt.$password);
				$user->setPassword($password);
				$this->sendEmail($email,'Account Reset','Your Account has been reset.');
			}
			else
			{
				if($password!='')
				{
					$password=md5($salt.$password);
					$user->setPassword($password);
					$user->setSalt($salt);
				}
			}
			$user->setStatus($status);
			$user->setTags($tags);
			$user->save();

			$this->createUserRoles($user, $roles);
			$this->createUserPermissions($user, $permissions);

			return $user;
		}

		/**
		 * add user's roles
		 *
		 * @access private
		 * @param      array  $roles  array of selected roles for the user
		 * @param      UserModel  $user   User object
		 * @return     void
		 */
		private function createUserRoles(UserModel $user, ?array<arraykey> $roles=null):void
		{
			if (!is_null($roles))
			{
				$userRole=$this->createUserRole();
				$roles=array_slice($roles, 0);
				foreach($roles as $role)
				{
					$userRole->setUser($user->getId());
					$userRole->setRole($role);
					$userRole->save();
				}
			}
		}

		/**
		 * Add user's permissions
		 * 
		 * @access private
		 * @param      array $permissions array of selected permissions for the user
		 * @param      UserModel $user User object
		 * @return     void 
		 */
		private function createUserPermissions(UserModel $user, ?array<arraykey> $permissions=null):void
		{
			if(!is_null($permissions))
			{
				$userPermission=$this->createUserPermission();
				$permissions=array_slice($permissions, 0);
				foreach($permissions as $permission)
				{
					$userPermission->setUserId($user->getId());
					$userPermission->setPermissionId($permission);
					$userPermission->save();
				}
			}
		}

		/**
		 * Generate random password
		 * 
		 * Get required condition from config and generate the password if user 
		 * select the option for random password
		 * 
		 * @access public
		 * @return string Random password
		 */
		public function generateRandomPassword():string
		{

			$policy = $this->getPolicyDetails();
			
			if(!is_null($policy['minLength']))
			{
				$minLength=$policy['minLength'];
			}
			else
			{
				$minLength=8;
			}
			if (function_exists('openssl_random_pseudo_bytes'))
			{
				$password=openssl_random_pseudo_bytes($minLength/2);
			}
			else
			{
				$process	=fopen('/dev/urandom','rb');
				$password	=fread($process,$minLength/2);
				fclose($process);
			}
			$password=bin2hex($password);

			if(!is_null($policy['symbol']))
			{
				if ($policy['symbol']==true)
				{
					$specials	=['!','@','#','$','%','^','&','*','-','+','_','='];
					$password	.=$specials[mt_rand(0,count($specials)-1)];
				}
			}

			if(!is_null($policy['number']))
			{
				if ($policy['number']==true && !preg_match('/[0-9]/',$password))
				{
					$numbers	=range(0,9);
					$password	.=$numbers[mt_rand(0,count($numbers)-1)];
				}
			}

			if(!is_null($policy['upperCase']))
			{
				if ($policy['upperCase']==true)
				{	
					if (!preg_match('/[a-z]/',$password))
					{
						$characters	=range('a','z');
						$password	.=$characters[mt_rand(0,count($characters)-1)];
					}
					if (!preg_match('/[A-Z]/',$password))
					{
						$characters	=range('A','Z');
						$password	.=$characters[mt_rand(0,count($characters)-1)];
					}
				}
			}
			
			$password=str_shuffle($password);
			return $password;
		}

		/**
		 * Generata salt value
		 * 
		 * Calculate the sha1 hash of a string with current time stamp
		 * 
		 * @access public
		 * @return string Salt
		 */
		public function generateSalt():string
		{
			return sha1('nuclio_ce1833cca4627da0751a2dcdde1f0b3b_'.time());
		}

		/**
		 * Generate confirmation code
		 * 
		 * @access public 
		 * @return     string return generaed code
		 */
		public function generateConfirmationCode():string
		{
			$length=20;
			if (function_exists('openssl_random_pseudo_bytes'))
			{
				$code=openssl_random_pseudo_bytes($length/2);
			}
			else
			{
				$process	=fopen('/dev/urandom','rb');
				$code		=fread($process,$length/2);
				fclose($process);
			}
			return bin2hex($code);
		}

		/**
		 * Validate user password
		 * 
		 * Validate user password based on detail in Policy config
		 * 
		 * @access public
		 * @param  Map<string,mixed> $record User record to be validate
		 * @throws     UserException If password length does not meet the policies
		 * @return     void
		 */
		private function validate(Map<string,mixed> $record):void
		{
			$policy			=$this->getPolicyDetails();
			$password		=$record->get('password');
			$passwordConfirm=$record->get('password_confirm');
			
			if($password=='')
			{
				return;
			}

			if($policy && !is_null($policy['forceRandom']))
			{
				if($policy['forceRandom']==false)
				{
					
					if($password != $passwordConfirm)
					{
						throw new UserException('Password and Password Confirm Does not Match');
					}

					$minLength		=$policy['minLength'];
					$maxLength		=$policy['maxLength'];
					$specialChars	=$policy['symbol'];
					$upperLowerChars=$policy['upperCase'];
					$numericDigits	=$policy['number'];

					//NEED TO CHANGE TO POLICY EXCEPTION LATER
					if ($minLength && strlen($password)<$minLength)
					{
						throw new UserException('Password must be at least "'.$minLength.'" characters in length.');
					}
					if ($maxLength && strlen($password)>$maxLength)
					{
						throw new UserException('Password cannot be more than "'.$maxLength.'" characters in length.');
					}
					$specials='!@#$%^&*_+-=';
					if ($specialChars && !preg_match('/['.$specials.']/',$password))
					{
						throw new UserException('Password must contain at least 1 special character ('.$specials.').');
					}
					if ($numericDigits && !preg_match('/[0-9]/',$password))
					{
						throw new UserException('Password must contain at least 1 numeric digit.');
					}
					if ($upperLowerChars)
					{
						if (!preg_match('/[a-z]/',$password))
						{
							throw new UserException('Password must contain at least 1 lower case character.');
						}
						if (!preg_match('/[A-Z]/',$password))
						{
							throw new UserException('Password must contain at least 1 upper case character.');
						}
					}
				}
			}
		}

		/**
		 * Get the policy details from Policy config
		 * 
		 * @access private
		 * @return     mixed   Policy details.
		 */
		private function getPolicyDetails(): mixed
		{
			$policy =$this->config->get('password');
			if (is_array($policy))
			{
				$policy=new Map($policy);
			}
			else if (!$policy instanceof Map)
			{
				$policy=Map{};
			}
			$policy =array_slice($policy,0,count($policy));

			return $policy;
		}

		/**
		 * Validate insert data
		 * 
		 * validate condition for insert action and throw except
		 * 
		 * @access private
		 * @param      Map<string,mixed> $record New user data
		 * @throws     UserException throw if password in empty
		 * @throws     UserException throw if user exists
		 * @return     void 
		 */
		private function insertValidation(Map<string,mixed> $record): void
		{
			$this->validate($record);

			$password	=(string)$record->get('password');
			$set_random	=$record->get('set_random');
			
			if($set_random==false && $password=='')
			{
				throw new UserException('Password cannot be empty',1);
			}

			$email		=(string)$record->get('email');

			$userExist=$this->dataSourceManager->findOne('user',Map{'email'=>$email});

			if($userExist)
			{
				throw new UserException('User already exists',2);
			}
		}

		/**
		 * Send email to user.
		 * 
		 * @access public
		 * @param      string $userMail User email address
		 * @param      string $subject Email subject
		 * @param      string $message Email content message
		 * @return      void
		 */
		public function sendEmail(string $userMail,string $subject,string $message):void
		{
			$email = Email::getInstance();
			$email->sendEmail
			(
				[
					$userMail
				], 
				$message,
				$subject 
			);
		}
	}
}